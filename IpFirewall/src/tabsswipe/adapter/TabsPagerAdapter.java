
package tabsswipe.adapter;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.houngedevelopment.ipfirewall.BlocklistFragment;
import com.houngedevelopment.ipfirewall.MainFragment;
import com.houngedevelopment.ipfirewall.SettingsFragment;

 
public class TabsPagerAdapter extends FragmentPagerAdapter {
 
    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }
 
    @Override
    public MainFragment getItem(int index) {
 
        switch (index) {
        case 0:
            // Main fragment activity
            return new MainFragment();
        case 1:
            // Blocklist fragment activity
            return new BlocklistFragment();
        case 2:
            // Settings fragment activity
            return new SettingsFragment();
        }
 
        return null;
    }
 
    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 3;
    }
 
}